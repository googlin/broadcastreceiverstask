package com.example.broadcastreceivingapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    final String LOG_TAG = "myLogs";

    public static final String CUSTOM_INTENT_ACTION = "com.example.broadcastreceivingapp.action.CUSTOM_INTENT_ACTION";
    public static final String CUSTOM_INTENT_EXTRA = "com.example.broadcastreceivingapp.broadcast.Message";
    public static final String ALARM_MESSAGE = "Alarm! Alarm! Alarm!";

    private Intent intent;
    private TextView textView1;
    private Button button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViewsById();

        button.setOnClickListener(v -> {
            textView1.setText(R.string.buttonClickedText);
            setBroadCastIntent();
            sendBroadcast(intent);
        });
    }

    private void findViewsById(){
        textView1 = findViewById(R.id.tvTask1);
        button = findViewById(R.id.btnStart);
    }

    private void setBroadCastIntent() {
        intent = new Intent(this, MessageReceiver.class);
        intent.setAction(CUSTOM_INTENT_ACTION);
        intent.putExtra(CUSTOM_INTENT_EXTRA, ALARM_MESSAGE);
    }
}

