package com.example.broadcastreceivingapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import static com.example.broadcastreceivingapp.MainActivity.CUSTOM_INTENT_EXTRA;

public class MessageReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //  This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        Toast.makeText(context,
                "Intent detected " +
                        intent.getStringExtra(CUSTOM_INTENT_EXTRA),
                Toast.LENGTH_LONG).show();
    }
}
